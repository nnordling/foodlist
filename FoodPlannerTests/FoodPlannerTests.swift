//
//  FoodListTests.swift
//  FoodListTests
//
//  Created by Niclas Nordling on 2021-03-24.
//

import XCTest
@testable import FoodPlanner

class FoodPlannerTests: XCTestCase {
    let onion = Ingredient(name: "Lök", quantity: 1)
    let meat = Ingredient(name: "Nötfärs", quantity: 1)
    
    override func tearDownWithError() throws {

//        Storage.shared.remove(.week)
//        Storage.shared.remove(.groceryList)
    }
    
    func test_getWeekFromUS_EmptyUS() {
        let week = Storage.shared.getWeek()
        
        XCTAssertEqual(week.count, 7)
        XCTAssertEqual(week[0].title, "Monday")
        XCTAssertTrue(week[1].food.ingredients.isEmpty)
        XCTAssertEqual(week[2].title, "Wednesday")
        XCTAssertTrue(week[3].food.ingredients.isEmpty)
    }
    
    func test_getWeekFromUS_NonEmptyUS() {
        
    }
    
    func test_getGroceryListFromUS_EmptyUS() {
//        let groceryList = Storage.shared.getGroceryList()
//
//        XCTAssertTrue(groceryList.isEmpty)
    }
    
    func test_getGroceryListFromUS_NonEmptyUS() {
        
    }
    
    func test_SaveWeekToUS() {
        
    }
    
    func test_SaveGroceryListToUS() {
        
    }
    
    func test_AddIngredientsToGroceryList() {
        Storage.shared.addToGroceryList(ingredients: [onion, meat])
        
        let groceryList = Storage.shared.getGroceryList()
        
        XCTAssertEqual(groceryList.count, 2)
        XCTAssertEqual(groceryList[0].name, "Lök")
        XCTAssertEqual(groceryList[0].quantity, 1)
    }
    
    func test_removeIngredientsFromGroceryList() {
//        Storage.shared.addToGroceryList(ingredients: [onion, meat])
//
//        Storage.shared.removeFromGroceryList(ingredients: [onion])
//
//        let groceryList = Storage.shared.getGroceryList()
//
//        XCTAssertEqual(groceryList.count, 1)
//        XCTAssertEqual(groceryList[0].name, "Nötfärs")
//        XCTAssertEqual(groceryList[0].quantity, 1)
    }

}
