//
//  Helper.swift
//  FoodPlanner
//
//  Created by Niclas Nordling on 2021-04-01.
//

import Foundation
import UIKit

class Helper {
    static let shared = Helper()
    
    func ingredientAlertController() -> UIAlertController {
        let alertController = UIAlertController(title: "Add Ingredient", message: nil, preferredStyle: .alert)
        alertController.addTextField()
        alertController.addTextField()
        let nameTextField = alertController.textFields?[0]
        let quantityTextField = alertController.textFields?[1]
        setupTextFields(for: [nameTextField, quantityTextField])
        
        return alertController
    }
    
    private func setupTextFields(for textFields: [UITextField?]) {
        for textField in textFields {
            textField?.autocapitalizationType = .sentences
            textField?.returnKeyType = .done
            textField?.enablesReturnKeyAutomatically = true
            textField?.clearButtonMode = .always
        }
        
        let nameTextField = textFields[0]
        nameTextField?.placeholder = "Name..."
        let quantityTextField = textFields[1]
        quantityTextField?.placeholder = "Quantity..."
        quantityTextField?.keyboardType = .numberPad
    }
}

protocol IngredientDelgate {
    func didAddIngredient(_ ingredient: Ingredient)
}
