//
//  Model.swift
//  FoodList
//
//  Created by Niclas Nordling on 2021-03-25.
//

import Foundation

public struct Food: Codable {
    var title: String
    var ingredients: [Ingredient]
    
    init(title: String, ingredients: [Ingredient]) {
        self.title = title
        self.ingredients = ingredients
    }
    
    init() {
        self.title = ""
        self.ingredients = [Ingredient]()
    }
}

public struct Weekday: Codable {
    var title: String
    var food: Food
}

public struct Ingredient: Codable {
    var name: String
    var quantity: Int
}

var testFoodItem = Food(title: "Pasta Alfredo", ingredients: [Ingredient(name: "Pasta", quantity: 1), Ingredient(name: "Parmesan", quantity: 2), Ingredient(name: "Sauce", quantity: 1)])

var monday = Weekday(title: "Monday", food: Food())
var tuesday = Weekday(title: "Tuesday", food: Food())
var wednesday = Weekday(title: "Wednesday", food: Food())
var thursday = Weekday(title: "Thursday", food: Food())
var friday = Weekday(title: "Friday", food: Food())
var saturday = Weekday(title: "Saturday", food: Food())
var sunday = Weekday(title: "Sunday", food: Food())

var testIngredients = [Ingredient(name: "Lök", quantity: 2), Ingredient(name: "Nötfärs", quantity: 1), Ingredient(name: "Krossade Tomater", quantity: 1), Ingredient(name: "Tomatpuré", quantity: 1), Ingredient(name: "Spagetti", quantity: 1)]

class Storage {
    static let shared = Storage()
    let userDefaults = UserDefaults.standard
    
    private var weekdays = [monday, tuesday, wednesday, thursday, friday, saturday, sunday]
    private var groceryList = [Ingredient]()
    
    func getWeek() -> [Weekday] {
        if let weekdaysData = UserDefaults.standard.value(forKey: "Week") as? Data {
            do {
                let weekDecoded = try PropertyListDecoder().decode([Weekday].self, from: weekdaysData)
                return weekDecoded
            } catch  {
                return Storage.shared.weekdays
            }
        } else {
            return Storage.shared.weekdays
        }
    }
    
    func getGroceryList() -> [Ingredient] {
        if let groceryListData = UserDefaults.standard.value(forKey: "GroceryList") as? Data {
            do {
                let groceryListDecoded = try PropertyListDecoder().decode([Ingredient].self, from: groceryListData)
                return groceryListDecoded
            } catch  {
                return groceryList
            }
        } else {
            return groceryList
        }
    }
    
    func save(_ week: [Weekday], for key: UserDefaultsKey) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(week), forKey: key.rawValue)
    }
    
    func save(_ groceryList: [Ingredient], for key: UserDefaultsKey) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(groceryList), forKey: key.rawValue)
    }
    
    func remove(_ key: UserDefaultsKey) {
        userDefaults.removeObject(forKey: key.rawValue)
    }
    
    func deleteGroceryList() {
        remove(.groceryList)
        groceryList = [Ingredient]()
    }
    
    func addToGroceryList(ingredients: [Ingredient]) {
        var ingredientExists = false
        var ingredientIndex = 0
        
        for newIngredient in ingredients {
            for (index, listIngredient) in groceryList.enumerated() {
                if newIngredient.name == listIngredient.name {
                    ingredientExists = true
                    ingredientIndex = index
                }
            }
            
            if ingredientExists {
                groceryList[ingredientIndex].quantity += newIngredient.quantity
            } else {
                groceryList.append(newIngredient)
            }
            ingredientExists = false
        }
        
        Storage.shared.save(groceryList, for: .groceryList)
    }
    
    func removeFromGroceryList(ingredients: [Ingredient]) {
        for ingredient in ingredients {
            for (index, listIngredient) in groceryList.enumerated() {
                if ingredient.name == listIngredient.name {
                    if ingredient.quantity == listIngredient.quantity {
                        groceryList.remove(at: index)
                    } else {
                        groceryList[index].quantity -= ingredient.quantity
                    }
                }
            }
        }
        
        Storage.shared.save(groceryList, for: .groceryList)
    }
    
    enum UserDefaultsKey: String {
        case week = "Week"
        case groceryList = "GroceryList"
    }
}
