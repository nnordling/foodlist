//
//  AddFoodViewController.swift
//  FoodList
//
//  Created by Niclas Nordling on 2021-03-25.
//

import UIKit

class PlanDinnerViewController: UIViewController {

    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var toolbar: UIToolbar!
    @IBOutlet weak private var titleTextField: UITextField!
    
    var food = Food()
    var week = [Weekday]()
    var weekdayIndex = 0
    var editMode = false
    
    private var oldIngredients = [Ingredient]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNibs()
        addToolbarItems()
        
        titleTextField.text = week[weekdayIndex].food.title
        oldIngredients = food.ingredients
    }
    
    private func registerNibs() {
        tableView.register(UINib(nibName: "IngredientsCell", bundle: nil), forCellReuseIdentifier: "IngredientsCell")
    }
    
    private func addToolbarItems() {
        let addIngredientButton = UIBarButtonItem(title: "Add Ingredient", style: .plain, target: self, action: #selector(addIngredient))
        let items = [addIngredientButton]
        toolbar.items = items
    }
    
    @IBAction private func saveFood() {
        saveOnExit()
        dismiss(animated: true)
    }
    
    @IBAction func closeView() {
        dismiss(animated: true)
    }
    
    @objc private func addIngredient() {
        let alertController = Helper.shared.ingredientAlertController()
        let nameTextField = alertController.textFields?[0]
        let quantityTextField = alertController.textFields?[1]
        
        let submit = UIAlertAction(title: "Submit", style: .default) { _ in
            if let ingredient = nameTextField?.text,
               let quantity = quantityTextField?.text {
                
                if !ingredient.isEmpty && !quantity.isEmpty {
                    self.food.ingredients.append(Ingredient(name: ingredient, quantity: Int(quantity) ?? 1))
                }
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(submit)
        alertController.addAction(cancel)

        present(alertController, animated: true)
    }
    
    func saveOnExit() {
        titleTextField.resignFirstResponder()
        week[weekdayIndex].food = food
        
        if editMode {
            Storage.shared.removeFromGroceryList(ingredients: oldIngredients)
        }

        Storage.shared.save(week, for: .week)
        
        Storage.shared.addToGroceryList(ingredients: food.ingredients)
    }
    
}

extension PlanDinnerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return food.ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let ingredientsCell = tableView.dequeueReusableCell(withIdentifier: "IngredientsCell") as? IngredientsCell else {
            print("IngredientsCell not found")
            return UITableViewCell()
        }
        
        ingredientsCell.textLabel?.text = food.ingredients[indexPath.row].name
        ingredientsCell.detailTextLabel?.text = "\(food.ingredients[indexPath.row].quantity)"
        return ingredientsCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Ingredients"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let ingredient = week[weekdayIndex].food.ingredients[indexPath.row]
            Storage.shared.removeFromGroceryList(ingredients: [ingredient])
            food.ingredients.remove(at: indexPath.row)
            week[weekdayIndex].food = food

            tableView.reloadData()
        }
    }
}

extension PlanDinnerViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            food.title = text
        }
    }
}
