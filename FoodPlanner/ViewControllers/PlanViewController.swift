//
//  PlanViewController.swift
//  FoodList
//
//  Created by Niclas Nordling on 2021-03-24.
//

import UIKit

class PlanViewController: UITableViewController {
    var week = Storage.shared.getWeek()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.register(UINib(nibName: "EmptyFoodCell", bundle: nil), forCellReuseIdentifier: "EmptyFoodCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        week = Storage.shared.getWeek()
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        
        if let target = segue.destination as? PlanDinnerViewController,
           let cell = tableView.cellForRow(at: indexPath) {
            target.weekdayIndex = indexPath.section
            target.food = week[indexPath.section].food
            target.week = week
            target.editMode = cell.reuseIdentifier == "EmptyCell" ? false : true
        }
    }
    
    @IBAction func clearUserDefault(_ sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Clear Week & Grocery List?", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Yes", style: .destructive) {_ in
            Storage.shared.remove(.week)
            Storage.shared.deleteGroceryList()
            self.week = Storage.shared.getWeek()
            self.tableView.reloadData()
        }
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        ac.addAction(action)
        ac.addAction(cancel)
        
        present(ac, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PlanViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return week.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if week[indexPath.section].food.title == "" {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyFoodCell") as? EmptyFoodCell {
                return cell
            } else {
                return UITableViewCell()
            }

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanCell", for: indexPath)
            cell.textLabel?.text = week[indexPath.section].food.title

            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "planDinnerSegue", sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Storage.shared.removeFromGroceryList(ingredients: week[indexPath.section].food.ingredients)
            week[indexPath.section].food = Food()
            Storage.shared.save(week, for: .week)
            tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return week[section].title
    }
}
