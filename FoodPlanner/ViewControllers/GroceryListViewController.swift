//
//  ShoppingListViewController.swift
//  FoodPlanner
//
//  Created by Niclas Nordling on 2021-03-27.
//

import UIKit

class GroceryListViewController: UITableViewController {

    @IBOutlet weak var toolbar: UIToolbar!
    
    var groceryList = Storage.shared.getGroceryList()
    var addedIngredients = [Ingredient]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        addToolbarItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        groceryList = Storage.shared.getGroceryList()
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Storage.shared.addToGroceryList(ingredients: addedIngredients)
        addedIngredients = [Ingredient]()
    }
    
    @IBAction func clearUserDefaults(_ sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Clear Grocery List?", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Yes", style: .destructive) {_ in
            Storage.shared.deleteGroceryList()
            self.groceryList = Storage.shared.getGroceryList()
            self.tableView.reloadData()
        }
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        ac.addAction(action)
        ac.addAction(cancel)
        
        present(ac, animated: true, completion: nil)
    }
    
    func addToolbarItems() {
        let addIngredientButton = UIBarButtonItem(title: "Add Ingredient", style: .plain, target: self, action: #selector(addIngredient))
        let items = [addIngredientButton]
        toolbar.items = items
    }
    
    @objc private func addIngredient() {
        let alertController = Helper.shared.ingredientAlertController()
        let nameTextField = alertController.textFields?[0]
        let quantityTextField = alertController.textFields?[1]
        
        let submit = UIAlertAction(title: "Submit", style: .default) { _ in
            if let name = nameTextField?.text,
               let quantity = quantityTextField?.text {
                
                if !name.isEmpty && !quantity.isEmpty {
                    let ingredient = Ingredient(name: name, quantity: Int(quantity) ?? 1)
                    self.groceryList.append(ingredient)
                    self.addedIngredients.append(ingredient)
                }
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(submit)
        alertController.addAction(cancel)
        
        present(alertController, animated: true)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groceryList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingListCell", for: indexPath)
        cell.textLabel?.text = groceryList[indexPath.row].name
        cell.detailTextLabel?.text = "\(groceryList[indexPath.row].quantity)"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.textLabel?.textColor = .systemGray
        cell?.detailTextLabel?.textColor = .systemGray
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Groceries"
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            groceryList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            Storage.shared.save(groceryList, for: .groceryList)
        }
    }
}
