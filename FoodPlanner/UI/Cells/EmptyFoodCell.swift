//
//  EmptyFoodCell.swift
//  FoodList
//
//  Created by Niclas Nordling on 2021-03-26.
//

import UIKit

class EmptyFoodCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
